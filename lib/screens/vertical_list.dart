import 'dart:math';
import 'package:flutter/material.dart';

class RandomListPage extends StatefulWidget {
  RandomListPage({Key? key}) : super(key: key);

  @override
  _RandomListPageState createState() => _RandomListPageState();
}

class _RandomListPageState extends State<RandomListPage> {
  final items = List<String>.generate(1000, (i) => "Item $i");

  Widget buildList() {
    return ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return Container(
            color: Color(Random().nextInt(0xffffffff)),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
              child: Text(items[index]),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Список'),
      ),
      body: buildList(),
    );
  }
}
