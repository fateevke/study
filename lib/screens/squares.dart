import 'dart:math';

import 'package:flutter/material.dart';

class SquaresPage extends StatefulWidget {
  SquaresPage({Key? key}) : super(key: key);

  @override
  _SquaresPageState createState() => _SquaresPageState();
}

class _SquaresPageState extends State<SquaresPage> {
  final Random _randomMethod = Random();

  Color _basicColor = Color(0xFFFFFFFF);

  Color changeColor() {
    setState(() {
      _basicColor = Color.fromARGB(
        _randomMethod.nextInt(256),
        _randomMethod.nextInt(256),
        _randomMethod.nextInt(256),
        _randomMethod.nextInt(256),
      );
    });
    return _basicColor;
  }

  Widget buildContainer(Color color) {
    return Container(
      color: color,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("4 квадрата"),
      ),
      body: GridView.count(
        padding: const EdgeInsets.all(10),
        crossAxisCount: 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        children: List.generate(4, (index) {
          return SizedBox(child: buildContainer(changeColor()));
        }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: changeColor,
        child: const Icon(Icons.coffee),
      ),
    );
  }
}
