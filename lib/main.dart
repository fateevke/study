import 'package:flutter/material.dart';
import 'package:flutter_study/screens/codelab.dart';
import 'package:flutter_study/screens/squares.dart';
import 'package:flutter_study/screens/vertical_list.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo',
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
        ),
      ),
      home: MyHomePage(),
      initialRoute: '/home',
      routes: {
        '/home': (context) => const MyHomePage(),
        '/squares': (context) => SquaresPage(),
        '/verticallist': (context) => RandomListPage(),
        '/codelab': (context) => CodelabPage(),
      },
    );
  }
}

class ButtonContentModel {
  final String buttonName;
  final String? route;
  ButtonContentModel({
    required this.buttonName,
    this.route,
  });
}

final List _buttonsContent = [
  ButtonContentModel(
    buttonName: '4 квадрата',
    route: '/squares',
  ),
  ButtonContentModel(buttonName: 'Цветной список', route: '/verticallist'),
  ButtonContentModel(buttonName: 'Codelab', route: '/codelab'),
  ButtonContentModel(buttonName: 'Асинхронный зарос', route: null),
];

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Главная'),
        ),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Column(
                children: _buttonsContent.map<Widget>((buttonItem) {
                  return LineButton(
                    buttonText: buttonItem.buttonName,
                    buttonRoute: buttonItem.route,
                  );
                }).toList(),
              ),
            ),
            Column()
          ],
        ));
  }
}

class LineButton extends StatefulWidget {
  final String buttonText;
  final String? buttonRoute;
  LineButton({
    Key? key,
    required this.buttonText,
    this.buttonRoute,
  }) : super(key: key);

  @override
  _LineButtonState createState() => _LineButtonState();
}

class _LineButtonState extends State<LineButton> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ElevatedButton(
            onPressed: (widget.buttonRoute != null)
                ? () {
                    Navigator.pushNamed(context, widget.buttonRoute!);
                  }
                : null,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Row(
                children: [
                  Text(widget.buttonText),
                  Expanded(child: Container()),
                  const Icon(Icons.arrow_forward),
                ],
              ),
            )),
        const SizedBox(
          height: 10,
        )
      ],
    );
  }
}
